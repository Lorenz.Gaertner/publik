import pyhf
import numpy as np
import matplotlib.pyplot as plt
from cycler import cycler
import seaborn as sns

from publik.reweight import utils

def pulls_wrt_nominal(pdf, bestfit):
    """
    TODO: add errorbars
    """
    pulls = utils.calculate_pulls_wrt_nominal(pdf, bestfit)
    
    labels= np.asarray([
        "{}[{}]".format(k, i) if pdf.config.param_set(k).n_parameters > 1 else k
        for k in pdf.config.par_order
        if pdf.config.param_set(k).constrained
        for i in range(pdf.config.param_set(k).n_parameters)
    ])
    
    order = np.argsort(labels)[::-1]
    # fitted = bestfit[order]
    labels = labels[order]
    pulls = pulls[order]

    plt.vlines([-2, 2], -0.5, len(pulls) - 0.5, colors="k", linestyles="dotted")
    plt.vlines([-1, 1], -0.5, len(pulls) - 0.5, colors="k", linestyles="dashdot")
    plt.fill_between([-2,2],[-0.5, -0.5], [len(pulls),len(pulls)], facecolor="yellow")
    plt.fill_between([-1,1],[-0.5, -0.5], [len(pulls),len(pulls)], facecolor="green")
    plt.vlines([0], -0.5, len(pulls) - 0.5, colors="k", linestyles="dashed")
    plt.scatter(pulls, range(len(pulls)), c="k")
    #plt.errorbar(
    #    range(len(pulls)), pulls, c="k", xerr=0, yerr=pullerr, marker=".", fmt="none"
    #)
    plt.yticks(range(len(pulls)), labels, fontsize=8)
    plt.gcf().set_size_inches(5, 20)
    plt.ylim(-0.5, len(pulls) - 0.5)
    plt.title("Constrained Parameters Pulls", fontsize=12)
    plt.xlabel(r"$(\hat{\theta} - \theta)\,/ \Delta \theta$", fontsize=12)
    
    
def projections(file, par, wcs, parlabel, lims=[], cut=0, contours=[]):  
    if not lims:
        lims = [min(par),max(par)]

    fig, ax = plt.subplots(1,3,figsize=(15,5), constrained_layout=True)
    cv = r'$C_{VL}^{NP} + C_{VR}$'
    cs = r'$C_{SL} + C_{SR}$'
    ct = r'$C_{TL}$'
    labels = [cv, cs, ct]

    for i in range(3):
        x = wcs[wcs[:,i]==cut][:,(i+1)%3]
        y = wcs[wcs[:,i]==cut][:,(i+2)%3]
        z = par[wcs[:,i]==cut]

        ax[i].set_title(labels[i%3] + f'={cut}')
        ax[i].set_xlabel(labels[(i+1)%3])
        ax[i].set_ylabel(labels[(i+2)%3])

        ax[i].set_box_aspect(1)
        levels = np.linspace(lims[0], lims[1], 20)
        cont = ax[i].tricontourf(x, y, z, levels=levels, cmap="plasma")
        if contours:
            opt = dict(levels=contours, colors=["white"])
            ax[i].tricontour(x, y, z, linestyles="dashed", **opt)
        ax[i].scatter(x, y, c=z, marker='.', edgecolor="tan", cmap="plasma")


    fig.colorbar(cont, ax=ax.ravel().tolist(), shrink=0.65, label=parlabel)

    plt.savefig(file)  
    plt.show()
    plt.close()
    
def yields(pdf, observations, par_name_dict, ax=None, order=[6,3,1,0,5,7,2,4],**par_settings):
    axsettings(ax)
    pars = pyhf.tensorlib.astensor(pdf.config.suggested_init())
    for k, v in par_settings.items():
        pars[par_name_dict[k]] = v
        
    mc_counts = pdf.main_model.expected_data(pars, return_by_sample=True)
    samples = pdf.config.samples
    bottom = None
    for i, sample_index in enumerate(order):
        data = mc_counts[sample_index]
        x = np.arange(len(data))
        ax.bar(x, data, 1, bottom=bottom, alpha=1.0, label=samples[sample_index])
        bottom = data if i == 0 else bottom + data
    errors = [np.sqrt(d) for d in observations]	
    ax.errorbar(
        x, observations, yerr=errors, fmt='o', c="k", alpha=1.0, zorder=99
    )
    
def axsettings(ax):
    ax.set_xlim((-1,24))
    
    helix_order=b2helix(7)[::-1]
    helix_order.append([1,0,0])
    my_color_cycle=cycler(color=helix_order)
    ax.set_prop_cycle(my_color_cycle)
    
    ax.axvline(x=8.5, color='black', linestyle='--',lw=1.0)
    ax.axvline(x=11.5, color='black', linestyle='--',lw=1.0)
    ax.axvline(x=20.5, color='black', linestyle='--',lw=1.0)
    
    ax.text(-0.45, 460, 'SR')
    ax.text(8.55, 460, 'CR1')
    ax.text(11.55, 460, 'CR2')
    ax.text(20.55, 460, 'CR3')
    
def yieldsSR(pdf, observations, par_name_dict, ax=None, order=[6,3,1,0,5,7,2,4], plot_obs=True, combine_samples=True, **par_settings):
    pars = pyhf.tensorlib.astensor(pdf.config.suggested_init())
    for k, v in par_settings.items():
        pars[par_name_dict[k]] = v
    
    bin_order = [9,10,11,0,1,2,3,4,5,6,7,8]
    mc_counts = pdf.main_model.expected_data(pars, return_by_sample=True)
    mc_counts = mc_counts[:,:12][:,bin_order]
    samples = pdf.config.samples
    
    if combine_samples:
        mc_counts, samples = combineSamples(mc_counts, samples)
        if len(order) != len(samples):  
            order = np.arange(4)[::-1]
            
    axsettingsSR(ax, Nsamples=len(samples))
                
    bottom = None
    for i, sample_index in enumerate(order):
        data = mc_counts[sample_index]
        x = np.arange(len(data))
        ax.bar(x, data, 1, bottom=bottom, alpha=1.0, label=samples[sample_index], edgecolor='black')
        bottom = data if i == 0 else bottom + data
    if plot_obs:
        observations = np.array(observations)[:12][bin_order]
        errors = np.array([np.sqrt(d) for d in observations])
        errors = errors[:12]
        ax.errorbar(
            x, observations, yerr=errors, fmt='o', c="k", alpha=1.0, zorder=99
        )
        
    annodateSR(ax)
    b2_logo(x=0.26, y=0.8, ax=ax)
            
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1], fontsize=12, bbox_to_anchor=(1, 0.95), loc='upper right')
    
def axsettingsSR(ax, Nsamples):
    ax.set_xlim((-0.5,11.5))
    ax.set_ylim((0,550))
    bins = np.arange(-0.5, 12, 1)
    labels = [0.5, 2.0, 2.4, r'$3.5 \mid 0.5$', 2.0, 2.4, r'$3.5 \mid 0.5$', 2.0, 2.4, r'$3.5 \mid 0.5$', 2.0, 2.4, 3.5]
    ax.set_xticklabels(labels, fontsize=9)
    ax.set_xticks(bins)
    ax.set_xlabel(r'$p_T(K^+)\,[GeV]$')
    ax.set_ylabel(r'Events')
    
    helix_order=b2helix(Nsamples-1)
    helix_order.append([1,0,0])
    my_color_cycle=cycler(color=helix_order)
    ax.set_prop_cycle(my_color_cycle)
    
    ax.axvline(x=2.5, color='black', linestyle='--',lw=1.0)
    ax.axvline(x=5.5, color='black', linestyle=':',lw=1.0)
    ax.axvline(x=8.5, color='black', linestyle=':',lw=1.0)
    
def annodateSR(ax):
    bdtsize=7
    ax.text(-0.4, ax.get_ylim()[1]*0.95, r'$0.93<BDT_2<0.95$', fontsize=bdtsize)
    ax.text(2.6, ax.get_ylim()[1] *0.95, r'$0.95<BDT_2<0.97$', fontsize=bdtsize)
    ax.text(5.6, ax.get_ylim()[1] *0.95, r'$0.97<BDT_2<0.99$', fontsize=bdtsize)
    ax.text(8.6, ax.get_ylim()[1] *0.95, r'$0.99<BDT_2$', fontsize=bdtsize)
    
    ax.text(-0.45, ax.get_ylim()[1]*1.01, 'CR1')
    ax.text(2.55, ax.get_ylim()[1]*1.01, 'SR')
    
def b2helix(n):
    ''' Color palette

    Taken from https://stash.desy.de/projects/BT/repos/b2plot/browse
    '''
    return sns.cubehelix_palette(n, start=1.5, rot=1.5, dark=0.3, light=.8, reverse=True)

def combineSamples(mc_counts, samples):
    '''
    Combine samples in the following way:
        Signal    = signal
        Neutral B = mixed
        Charged B = charged
        Continuum = ddbar, uubar, ssbar, ccbar, taupair
    '''
    mc_counts = np.array(mc_counts)
    samples = np.array(samples)
    
    samples_new = [r'$B^+ \to K^+ \nu \bar \nu$', 'Neutral B', 'Charged B', 'Continuum']
    
    mc_new = np.zeros((4, np.shape(mc_counts)[1]))
    mc_new[0] = mc_counts[samples=='signal'][0]
    mc_new[1] = mc_counts[samples=='mixed'][0]
    mc_new[2] = mc_counts[samples=='charged'][0]
    mc_new[3] = mc_counts[samples=='ddbar'][0]
    mc_new[3] += mc_counts[samples=='uubar'][0]
    mc_new[3] += mc_counts[samples=='ssbar'][0]
    mc_new[3] += mc_counts[samples=='ccbar'][0]
    mc_new[3] += mc_counts[samples=='taupair'][0]
    
    return mc_new, samples_new

def b2_logo(
        x=0.3,
        y=0.5,
        fontsize=12,
        is_data=True,
        lumi='63+9',
        preliminary=False,
        two_lines=True,
        ax=None,
        **kwargs):
    '''
    plot the Belle II logo and the integrated luminosity (or "Simulation").

    Parameters
    ----------
    x : x position
    y : y position
    fontsize : fontsize
    is_data : if True, plot int. luminosity. If False, plot "Simulation".
    lumi : Integrated luminosity in fb-1 as a string. Default value is "63+9". If empty, do not plot luminosity.
    preliminary : If True (default), print preliminary
    two_lines : If True (default), write the information on two lines
    ax : figure axis
    kwargs : kwargs to be passed to the text function
    '''
    if ax is None:
        ax = plt.gca()
    transform = ax.transAxes

    s = r'$\mathrm{\mathbf{Belle\,\,II}' + (r'\,\,preliminary}$' if preliminary else '}$')
    if two_lines:
        s += '\n'
    else:
        s += ' '
    if is_data:
        if lumi:
            s += r'$\int\,\mathcal{L}\,\mathrm{d}t=' + lumi + r'\,\mathrm{fb}^{-1}$'
    else:
        s += r'$\mathrm{\mathbf{simulation}}$'

    ax.text(x, y, s, fontsize=fontsize, transform=transform, **kwargs)