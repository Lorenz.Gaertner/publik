import numpy as np
import json
import jsonschema
import requests
import pyhf

##################################################################################################
# FIT

def getPDF(file, return_obs=False, validate=True, modifier_set=None):
    """Get the pyhf model from an input json file

    Args:
        file (sting): path to the input json
        return_obs (bool, optional): whether to return the observed data. Defaults to False.
        validate (bool, optional): whether to apply the validation procedure. Defaults to True.
        modifier_set (_type_, optional): provide an extension to the default pyhf modifier set. Defaults to None.

    Returns:
        pdf: pyhf model
        data: the observations, including auxiliary data
        observations: the observations, excluding auxiliary data
    """
    #Load json file
    with open(file, 'rb') as f:
        json_workspace = json.load(f) 
    del json_workspace['content'] #binning needs to be deleted from json file so that validation below works
    
    if validate:
        schema = requests.get('https://scikit-hep.org/pyhf/schemas/1.0.0/workspace.json').json()
        resolver = jsonschema.RefResolver(base_uri = 'https://scikit-hep.org/pyhf/schemas/1.0.0/', referrer = schema)
        jsonschema.validate(instance=json_workspace, schema=schema, resolver=resolver) #validation
    
    #Load pdf from json file
    if modifier_set:
        pdf=pyhf.Model({"channels":json_workspace["channels"]}, 
                        modifier_set=modifier_set,
                        validate = validate,
                        batch_size = None)
    else: # modifier_set, validate not available in 0.5.4
        pdf=pyhf.Model({"channels":json_workspace["channels"]}, 
                        batch_size = None, validate = validate) 	

    #Load data from json file
    observations = []
    for i in range(len(json_workspace['observations'])):
        observations += json_workspace['observations'][i]['data']
    data = pyhf.tensorlib.astensor(observations + pdf.config.auxdata)
    
    if return_obs:
        return pdf, data, observations
    return pdf, data

def calculate_pulls_wrt_nominal(pdf, bestfit):
    """Calculate the pulls wrt the nominal parameters

    Args:
        pdf (_type_): pyhf model
        bestfit (_type_): fit parameters

    Returns:
        _type_: _description_
    """
    pulls = pyhf.tensorlib.concatenate([
        (bestfit[pdf.config.par_slice(k)] - pdf.config.param_set(k).suggested_init)
        / pdf.config.param_set(k).width()
        for k in pdf.config.par_order
        if pdf.config.param_set(k).constrained
    ])

    return pulls

def getQ2Distributions(file):
    with open(file, 'rb') as f:
        jsonPub = json.load(f)
    binningsr = jsonPub['content'][0]['binning3D']
    data3dsr = np.reshape(jsonPub['channels'][0]['samples'][0]['data'], [len(binningsr[i])-1 for i in range(len(binningsr))], order='F')
    binningcr1 = jsonPub['content'][1]['binning3D']
    data3dcr1 = np.reshape(jsonPub['channels'][1]['samples'][0]['data'], [len(binningcr1[i])-1 for i in range(len(binningcr1))], order='F')
    binningcr2 = jsonPub['content'][0]['binning3D']
    data3dcr2 = np.reshape(jsonPub['channels'][2]['samples'][0]['data'], [len(binningcr2[i])-1 for i in range(len(binningcr2))], order='F')
    binningcr3 = jsonPub['content'][1]['binning3D']
    data3dcr3 = np.reshape(jsonPub['channels'][3]['samples'][0]['data'], [len(binningcr3[i])-1 for i in range(len(binningcr3))], order='F')

    q2dist = np.reshape(np.concatenate((np.swapaxes(data3dsr, 0, 1), 
                                        np.swapaxes(data3dcr1,0,1), 
                                        np.swapaxes(data3dcr2,0,1), 
                                        np.swapaxes(data3dcr3,0,1)), 
                                       axis=0), (24,24))
    return q2dist, np.array(binningsr[-1])