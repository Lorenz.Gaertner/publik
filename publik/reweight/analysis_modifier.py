from publik.weights import utils
from publik.reweight import custom_modifier


import numpy as np

def add(q2binning, q2dist, norm = None):
    """Add modifier to the expanded modifier set for pyhf. This function adds the new modifier.

    Args:
        q2binning (_type_): the q2 binning
        q2dist (_type_): the q2 distribution
        norm (_type_): the normalization factor of the weights. Default: (phase space) * (SM branching ratio)

    Returns:
        _type_: the expanded modifier set
    """
    
    if norm is None:
        # Phase space normalization
        PHSP = utils.computePHSP(q2binning)
        norm = PHSP
    
    new_params = {
                'ffpca0':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca1':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca2':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
            }

    cmod = weight_modifier(q2binning, q2dist, norm)
    external_func= cmod.weights
    expanded_pyhf = custom_modifier.add('customfunc', list(new_params.keys()), new_params, namespace = {'external_func': external_func})
    return expanded_pyhf

class weight_modifier():
    def __init__(self, q2binning, q2dist, norm):
        """
        This modifier computes the bin modifications for given Wilson coefficients and hadronic parameters.
        The standard hadronic parameters are used here.
        
        Inputs:
            q2binning: q2 binning, cut to phase space
            q2dist: q2 bistribution in analysis bin
            norm: normalization of weights
        """
        self.q2binning = q2binning
        self.q2dist = q2dist
        self.norm = norm
        
        # parameters
        self.mB = 5.27934
        self.mK = 0.493677
        
        # get the mean hadronic parameters
        self.alpha_mean = [0.432,-0.664,-1.20] # eq 107, central values
        # get the covariance matrix of the hadronic parameters
        ae = np.array([0.011,0.096,0.69])  # eq 107, uncertainties
        cor = np.array([[1.0,0.32,-0.37],[0.32,1.,0.26],[-0.37,0.26,1.]])  # eq 108, correlation matrix
        cov = ae[:,np.newaxis]*ae[np.newaxis,:]*cor   # covariance
        # use the covariance to get the correctly scaled pca eigenvectors
        self.uvec = utils.pca(cov)
        
        # cache previously called function values
        self.cache = {}
        
    def getalpha(self, pars):
        # map from pca parameters to hadronic parameters
        pca_shifts = np.array([pars[f'ffpca{i}'] for i in range(3)])
        had_shifts = self.uvec @ pca_shifts
        return self.alpha_mean + had_shifts
    
    def getweights(self, alpha):
        BR = utils.bintegrate(self.pk, self.q2binning, args=(alpha))
        BR = BR / sum(BR)
        weights = BR / self.norm        
        weights[weights<0] = 1.
        weights[np.isnan(weights)] = 1.
        return weights
        
    def weights(self, pars):
        key = tuple(i for i in pars.items())
        if key in self.cache:
            return self.cache[key]
        
        # get the mean + shifts in the hadronic parameters
        alpha = self.getalpha(pars)
        weights = self.getweights(alpha)
        
        def func(ibins):
            results = np.sum((weights-1)*self.q2dist, axis=-1)
            return np.array([results[i] for i in ibins])
        
        self.cache[key] = func
        
        return func
    
    def z(self, t):
        ''' Z-parameterizaion, eq 105'''
        tm = (self.mB-self.mK)**2
        tp = (self.mB+self.mK)**2
        t0 = tp*(1-np.sqrt(1-tm/tp))
        return (np.sqrt(tp-t) - np.sqrt(tp-t0)) / (np.sqrt(tp-t) +np.sqrt(tp-t0))
    
    def fplus(self, q2,a):
        '''f+ form factor, depends on q2 and a-parameters (z-parameterization), eq 104'''
        mP = self.mB+0.046 # two lines after eq 105
        return 1/(1-q2/mP**2)*(a[0]
                            +a[1]*self.z(q2)
                            +a[2]*self.z(q2)**2
                            +self.z(q2)**3/3*(-a[1]+2*a[2])
                            )
    def lam(self, a,b,c):
        ''' useful for Dalitz plot integration, eq. 102'''
        return a**2+b**2+c**2-2*(a*b+b*c+a*c)

    def pk(self, q2,a): 
        ''' Return dsigma / dq2, including form factor, eq 109 '''
        return self.lam(self.mB**2,self.mK**2,q2)**(3/2)/self.mB**4*(self.fplus(q2,a))**2

    def pkf(self, q2,a): 
        ''' Return dsigma / dq2, no form factor'''
        return self.lam(self.mB**2,self.mK**2,q2)**(3/2)/self.mB**4
