from publik.weights import utils
from publik.reweight import custom_modifier

import numpy as np

def add(q2binning, q2dist, norm = None):
    """Add modifier to the expanded modifier set for pyhf. This function adds the new modifier.

    Args:
        q2binning (_type_): the q2 binning
        q2dist (_type_): the q2 distribution
        norm (_type_): the normalization factor of the weights. Default: (phase space) * (SM branching ratio)

    Returns:
        _type_: the expanded modifier set
    """
    
    if norm is None:
        # Phase space normalization
        PHSP = utils.computePHSP(q2binning)
        # SM normalization
        BRSM = utils.computeBR()
        norm = PHSP * BRSM
    
    new_params = {
                'cvl'   :{'inits': (0.0,), 'bounds': ((-35.0,35.0),), 'paramset_type': 'unconstrained'},
                'csl'   :{'inits': (0.0,), 'bounds': ((-35.0,35.0),), 'paramset_type': 'unconstrained'},
                'ctl'   :{'inits': (0.0,), 'bounds': ((-35.0,35.0),), 'paramset_type': 'unconstrained'},
                'ffpca0':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca1':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca2':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca3':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca4':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca5':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca6':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
                'ffpca7':{'inits': (0.0,), 'bounds': ((-5.0, 5.0),) , 'paramset_type': 'constrained_by_normal'},
            }

    cmod = weight_modifier_syst(q2binning, q2dist, norm)
    external_func= cmod.weights
    expanded_pyhf = custom_modifier.add('customfunc', list(new_params.keys()), new_params, namespace = {'external_func': external_func})
    return expanded_pyhf

class weight_modifier():
    def __init__(self, q2binning, q2dist, norm):
        """
        This modifier computes the bin modifications for given Wilson coefficients and hadronic parameters.
        The standard hadronic parameters are used here.
        
        Inputs:
            q2binning: q2 binning, cut to phase space
            q2dist: q2 bistribution in analysis bin
            norm: normalization of weights
        """
        self.q2binning = q2binning
        self.q2dist = q2dist
        self.norm = norm
        
        self.analysis = utils.analysis()
        self.default_parameters = self.analysis.clone().parameters
        
        # cache previously called function values
        self.cache = {}
        
    def getalpha(self, pars):
        ffpars = ['f+_0', 'f+_1', 'f+_2', 'f0_1', 'f0_2', 'fT_0', 'fT_1', 'fT_2']
        if all(key in pars for key in ffpars):
            return [pars[k] for k in ffpars]
        else:
            return []
        
    def weights(self, pars):
        key = tuple(i for i in pars.items())
        if key in self.cache:
            return self.cache[key]
        
        cvl = pars['cvl']
        csl = pars['csl']
        ctl = pars['ctl']
        
        # get the mean + shifts in the hadronic parameters
        alpha = self.getalpha(pars)
        
        #reset parameters
        utils.parameterReset(self.analysis.parameters, self.default_parameters)
        
        # compute the new weights and process them for sensibility
        weights = utils.computeWeights({'cVL': cvl, 'cSL': csl, 'cTL': ctl}, 
                                           self.q2binning, 
                                           norm=self.norm, 
                                           alpha=alpha, 
                                           parameters=self.analysis.parameters)
        weights[weights<0] = 1.
        weights[np.isnan(weights)] = 1.
        
        def func(ibins):
            results = np.sum((weights-1)*self.q2dist, axis=-1)
            return np.array([results[i] for i in ibins])
        
        self.cache[key] = func
        
        return func
    
class weight_modifier_syst(weight_modifier):
    def __init__(self, q2binning, q2dist, norm):
        """
        This modifier computes the bin modifications for given Wilson coefficients and hadronic parameters.
        The hadronic parameters are decorrelated using the covariance matrix obtained from EOS sampling.
        The modifier parameters of the hadronic parameters are then the scalings for the pca eigenvectors.
        
        inputs:
            q2binning: q2 binning, cut to phase space
            q2dist: q2 bistribution in analysis bin
            norm: normalization of weights
        """
        
        super().__init__(q2binning, q2dist, norm)

        # sample parameters
        par, _ = utils.sampledBR(self.q2binning)
        # get the mean hadronic parameters
        self.alpha_mean = np.mean(par, axis=0)
        # get the covariance matrix of the hadronic parameters
        cov = np.cov(par.T)
        # use the covariance to get the correctly scaled pca eigenvectors
        self.uvec = utils.pca(cov)
        
    def getalpha(self, pars):
        # map from pca parameters to hadronic parameters
        pca_shifts = np.array([pars[f'ffpca{i}'] for i in range(8)])
        had_shifts = self.uvec @ pca_shifts
        return self.alpha_mean + had_shifts