#create input file to specify models for grid scan of Wilson coeffs

import os
from pathlib import Path

import numpy as np
import json

def WCinput(cVLs, cSLs, cTLs):
    """_summary_

    Args:
        cVLs (array): values for grid scan
        cSLs (array): values for grid scan
        cTLs (array): values for grid scan

    Returns:
        list: models for all points on the grid
    """
    WCs = []

    for cvl in cVLs:
        WCs.append({'cvl': cvl})
    for csl in cSLs:
        WCs.append({'csl': csl})
    for ctl in cTLs:
        WCs.append({'ctl': ctl})

    for cvl in cVLs:
        for csl in cSLs:
            WCs.append({'cvl': cvl, 
                        'csl': csl})
    for cvl in cVLs:
        for ctl in cTLs:
            WCs.append({'cvl': cvl, 
                        'ctl': ctl})
    for csl in cSLs:
        for ctl in cTLs:
            WCs.append({'csl': csl, 
                        'ctl': ctl})

    return WCs
        
            
if __name__ == '__main__':
    BASE = Path(os.path.dirname(__file__))
    
    # cVLs = [0.0]
    # cSLs = [0.0]
    # cTLs = [0.0]
    cVLs = np.linspace(-35., 35., 70+1)
    cSLs = np.linspace(-35., 35., 70+1)
    cTLs = np.linspace(-35., 35., 70+1)
    
    WCs = WCinput(cVLs, cSLs, cTLs)
    print(len(WCs), 'models')
    
    with open(BASE / f'inputs/scan.json', 'w') as ff:
        jstr = json.dumps(WCs, indent=4)
        ff.write(jstr)
