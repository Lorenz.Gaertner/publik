import os
from pathlib import Path

import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
import matplotlib

matplotlib.style.use(Path(os.path.dirname(__file__)) / '../../style.mplstyle')

from matplotlib import rc

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

from publik.weights import utils

# #PLOT
# ###############################################################################################

def counts(file, ids, q2_pts, dbrs, dbrerrors):
    q2_pts_cent =  (q2_pts[:-1] + q2_pts[1:])/2

    fig, ax = plt.subplots(1,1,figsize=(8,5))

    scalePHSP = 1/sp.integrate.quad(utils.analyticDBR, q2_pts[0], utils.kinlim())[0]
    ax.plot(q2_pts_cent, utils.analyticDBR(q2_pts_cent)*scalePHSP, 'm-', label='Phase space')
    
    plt.gca().set_prop_cycle(None)
    i=0
    for dbr,err,id in zip(dbrs,dbrerrors,ids):
        id = id.replace('cVL', r'$C_{VL}^{NP}$')
        id = id.replace('cSL', r'$C_{SL}$')
        id = id.replace('cTL', r'$C_{TL}$')
        ax.errorbar(q2_pts_cent, dbr, xerr=(q2_pts_cent[1]-q2_pts_cent[0])/2, yerr=err,  label=f'{id}', fmt=f'oC{i%9}')    
        i+=1

    ax.set_xlabel(r'$q^2$ [GeV$^2$]')
    ax.set_xlim(0., 23.)
    ax.set_ylabel(r'Number density')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    fig.savefig(file)
    plt.close()
    
def samples(file, ids, q2_pts, samples, density=True, phsp=True, **kwargs):
    q2_pts_cent =  (q2_pts[:-1] + q2_pts[1:])/2

    fig, ax = plt.subplots(1,1,figsize=(8,5))

    if phsp:
        scalePHSP = 1/sp.integrate.quad(utils.analyticDBR, q2_pts[0], utils.kinlim())[0]
        ax.plot(q2_pts_cent, utils.analyticDBR(q2_pts_cent)*scalePHSP, 'm-', label='Phase space')
    
    #reset color cycle
    plt.gca().set_prop_cycle(None)
    
    # pick subset of curves
    i=0
    for sam,id in zip(samples,ids):
        id = id.replace('cVL', r'$C_{VL}^{NP}$')
        id = id.replace('cSL', r'$C_{SL}$')
        id = id.replace('cTL', r'$C_{TL}$')
        id = id.split('=')[0] + '=' + str(float(id.split('=')[1]))
        
        pt = 'o'
        color = f'C{i%9}'
        
        #if SM
        if id == r'$C_{VL}^{NP}$=0.0':
            id = 'SM'
            pt = 'X'
            color = 'g'
        
        ind = np.random.randint(0, len(sam), 100)
        for s in sam[ind]:
            ax.plot(q2_pts_cent, s, color, alpha=0.05)    
        ax.errorbar(q2_pts_cent, 
                    np.mean(sam, axis=0), 
                    xerr=(q2_pts_cent[1]-q2_pts_cent[0])/2, 
                    yerr=np.std(sam, axis=0), 
                    fmt= pt + color, 
                    label=f'{id}')
        i+=1

    if 'xlabel' in kwargs: 
        ax.set_xlabel(kwargs['xlabel'])
    if 'ylabel' in kwargs: 
        ax.set_ylabel(kwargs['ylabel'])
    ax.set_xlim(0., 23.)
    
    handles, labels = plt.gca().get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), reverse=True))
    ax.legend(handles, labels, loc='center left', bbox_to_anchor=(1, 0.5))

    fig.savefig(file)
    plt.close()
    
def weights(file, ids, q2_pts, weights, errors=[]):
    
    q2_pts_cent =  (q2_pts[:-1] + q2_pts[1:])/2

    fig, ax = plt.subplots(1,1,figsize=(8,5))

    for c,e,id in zip(weights, errors, ids):
        id = id.replace('cVL', r'$C_{VL}^{NP}$')
        id = id.replace('cSL', r'$C_{SL}$')
        id = id.replace('cTL', r'$C_{TL}$')
        ax.errorbar(q2_pts_cent, c, xerr=(q2_pts_cent[1]-q2_pts_cent[0])/2, yerr=e,  label=f'{id}', fmt='o')

    ax.set_xlabel(r'$q^2$ [GeV$^2$]')
    ax.set_xlim(0., 23.)
    #ax.set_ylim(0., 4.)
    ax.set_ylabel(r'$d\mathcal{B}(WET)/d\mathcal{B}(PHSP)$')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    fig.savefig(file)
    plt.close()

# ###############################################################################################

def weight(file, id, q2_pts, weights, ups, downs):
    q2_pts_cent =  (q2_pts[:-1] + q2_pts[1:])/2

    fig, ax = plt.subplots(1,1,figsize=(8,5))
    
    #reset color cycle
    plt.gca().set_prop_cycle(None)
    
    ax.plot(q2_pts_cent, weights,'ok', label = id)

    #plot variations
    i = 0
    for u,d in zip(ups, downs):
        ax.plot(q2_pts_cent, u, f'2C{i}', label = f'up {i}')
        ax.plot(q2_pts_cent, d, f'1C{i}', label = f'down {i}')
        i += 1

    ax.set_xlabel(r'$q^2$ [GeV$^2$]')
    ax.set_xlim(0., 23.)
    #ax.set_ylim(0., 4.)
    ax.set_ylabel(r'$d\mathcal{B}(WET)/d\mathcal{B}(PHSP)$')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    fig.savefig(file)
    plt.close()
    
def weightAll(file, ids, q2_pts, weights, corrup, corrdown):
    q2_pts_cent =  (q2_pts[:-1] + q2_pts[1:])/2
    
    fig, ax = plt.subplots(1,1,figsize=(8,5))

    #reset color cycle
    #plt.gca().set_prop_cycle(None)
    i = 0
    for id, w, ups, downs in zip(ids, weights, corrup, corrdown):
        ax.plot(q2_pts_cent, w, f'oC{i}', label = id)

        #plot variations
        for u,d in zip(ups, downs):
            ax.plot(q2_pts_cent, u, f'2C{i}')
            ax.plot(q2_pts_cent, d, f'1C{i}')
        
        i += 1

    ax.set_xlabel(r'$q^2$ [GeV$^2$]')
    ax.set_xlim(0., 23.)
    #ax.set_ylim(0., 4.)
    ax.set_ylabel(r'$d\mathcal{B}(EOS)/d\mathcal{B}(PHSP)$')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    
    fig.savefig(file)
    plt.close()
