import eos
import uproot
import numpy as np
import scipy as sp
from publik.tools.numpy_lru_cache_decorator import np_cache
#EOS messes up something oterwise
#plt.style.use('style.mplstyle')

# plt.style.use('../style.mplstyle')
# plt.rcParams["pgf.texsystem"] = ("pdflatex")


###############################################################################################
# EOS
def analysis():
    """
    Specify the likelihoods and FF parameter ranges 
    
    Returns:
        EOS analysis instance
    """

    # form factor expansion f+_0,1,2 are expansion parameters up to 2nd order
    # there is no f0_0 because of a constriant which removes one parameter

    parameters = [0.33772497529184886, -0.87793473613271, -0.07935870922121949, 0.3719622997220613, 0.07388594710238389, 0.327935912834808, -0.9490004115927961, -0.23146429907794228]
    paramerror = [0.010131234226468245, 0.09815140228051167, 0.26279803480131697, 0.07751034526769873, 0.14588095119443809, 0.019809720318176644, 0.16833757660616938, 0.36912754148836896]
    sigma = 15
    analysis_args = {
        'priors': [
            { 'parameter': 'B->K::alpha^f+_0@BSZ2015', 'min': parameters[0]-sigma*paramerror[0], 'max': parameters[0]+sigma*paramerror[0], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f+_1@BSZ2015', 'min': parameters[1]-sigma*paramerror[1], 'max': parameters[1]+sigma*paramerror[1], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f+_2@BSZ2015', 'min': parameters[2]-sigma*paramerror[2], 'max': parameters[2]+sigma*paramerror[2], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f0_1@BSZ2015', 'min': parameters[3]-sigma*paramerror[3], 'max': parameters[3]+sigma*paramerror[3], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f0_2@BSZ2015', 'min': parameters[4]-sigma*paramerror[4], 'max': parameters[4]+sigma*paramerror[4], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^fT_0@BSZ2015', 'min': parameters[5]-sigma*paramerror[5], 'max': parameters[5]+sigma*paramerror[5], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^fT_1@BSZ2015', 'min': parameters[6]-sigma*paramerror[6], 'max': parameters[6]+sigma*paramerror[6], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^fT_2@BSZ2015', 'min': parameters[7]-sigma*paramerror[7], 'max': parameters[7]+sigma*paramerror[7], 'type': 'uniform' }
        ],
        'likelihood': [
            'B->K::f_0+f_++f_T@FLAG:2021A', #ONLY IN v1.0.3
            'B->K::f_0+f_++f_T@HPQCD:2022A'
        ]
    }

    analysis = eos.Analysis(**analysis_args)
    analysis.optimize()
    return analysis

def parameterReset(parameters, default_parameters):
    """Reset the EOS parameters to the default values
    (This is a workaround for a bug in EOS, where the parameter objects are not deleted, causing a memory leak)

    Args:
        parameters (_type_): parameter object to be resetted
        default_parameters (_type_): default parameters
    """
    parameters['sbnunu::Re{cVL}'].set(default_parameters['sbnunu::Re{cVL}'].evaluate())
    parameters['sbnunu::Re{cVR}'].set(default_parameters['sbnunu::Re{cVR}'].evaluate())
    parameters['sbnunu::Re{cSL}'].set(default_parameters['sbnunu::Re{cSL}'].evaluate())
    parameters['sbnunu::Re{cSR}'].set(default_parameters['sbnunu::Re{cSR}'].evaluate())
    parameters['sbnunu::Re{cTL}'].set(default_parameters['sbnunu::Re{cTL}'].evaluate())
    parameters['B->K::alpha^f+_0@BSZ2015'].set(default_parameters['B->K::alpha^f+_0@BSZ2015'].evaluate())
    parameters['B->K::alpha^f+_1@BSZ2015'].set(default_parameters['B->K::alpha^f+_1@BSZ2015'].evaluate())
    parameters['B->K::alpha^f+_2@BSZ2015'].set(default_parameters['B->K::alpha^f+_2@BSZ2015'].evaluate())
    parameters['B->K::alpha^f0_1@BSZ2015'].set(default_parameters['B->K::alpha^f0_1@BSZ2015'].evaluate())
    parameters['B->K::alpha^f0_2@BSZ2015'].set(default_parameters['B->K::alpha^f0_2@BSZ2015'].evaluate())
    parameters['B->K::alpha^fT_0@BSZ2015'].set(default_parameters['B->K::alpha^fT_0@BSZ2015'].evaluate())
    parameters['B->K::alpha^fT_1@BSZ2015'].set(default_parameters['B->K::alpha^fT_1@BSZ2015'].evaluate())
    parameters['B->K::alpha^fT_2@BSZ2015'].set(default_parameters['B->K::alpha^fT_2@BSZ2015'].evaluate())

def sampledBR(q2_pts, model={}, ana = None, N_sample = 10000):
    """
    Sample the binned differential branching ratios in EOS, given a dict of Wilson coefficients to set. 
    
    Inputs:
        model: Dictionary setting the Wilson coefficients. 
                Possible entires: 'cVL', 'cVR', 'cSL', 'cSR', 'cTL'
        q2_pts: The q2 bin edges to compute the branching ratio in.
        
    Returns:
        (tuple): (FF parameter variations, BR samples in the given binning)
    """
    if not ana:
        ana = analysis()
    
    #set model
    cVLSM = ana.parameters['sbnunu::Re{cVL}'].evaluate()
    for key, value in model.items():
        if key == 'cVL':
            ana.parameters[f'sbnunu::Re{{{key}}}'].set(cVLSM+value)
        else:
            ana.parameters[f'sbnunu::Re{{{key}}}'].set(value)

    #Sample
    obs  = [eos.Observable.make(
                'B->Knunu::BR',
                ana.parameters,
                eos.Kinematics(q2_min=q2min, q2_max=q2max),
                eos.Options({'form-factors':'BSZ2015', 'model':'WET'}))
                for q2min, q2max in zip(q2_pts[:-1], q2_pts[1:]) ]
    
    par, _, sam = ana.sample(N=N_sample, pre_N=1000, stride=10, observables=obs)
    return par, sam

def computedBR(model, q2_pts, alpha = [], parameters = None):
    """
    Compute differential brnaching ratio as a function of the model, the FF parameters and the q2 bin edges

    Args:
        model: dictionary of WCs. Possible WCs: CVL, CVR, CSL, CSR, CTL
        q2_pts: q2 bin edges. Must be within kinematic limit of [0.0, ~22.9]
        alphas: array of FF parameters
        parameters: EOS parameters object

    Returns:
        array of branching ratios in given binning 
    """
    if not parameters:
        ana = analysis()
        p = ana.parameters
    else:
        p = parameters
        
    if len(alpha)!=0: 
        p['B->K::alpha^f+_0@BSZ2015'].set(alpha[0])
        p['B->K::alpha^f+_1@BSZ2015'].set(alpha[1])
        p['B->K::alpha^f+_2@BSZ2015'].set(alpha[2])
        p['B->K::alpha^f0_1@BSZ2015'].set(alpha[3])
        p['B->K::alpha^f0_2@BSZ2015'].set(alpha[4])
        p['B->K::alpha^fT_0@BSZ2015'].set(alpha[5])
        p['B->K::alpha^fT_1@BSZ2015'].set(alpha[6])
        p['B->K::alpha^fT_2@BSZ2015'].set(alpha[7])
    
    #set model
    cVLSM = p['sbnunu::Re{cVL}'].evaluate()

    for key, value in model.items():
        if key == 'cVL':
            p[f'sbnunu::Re{{{key}}}'].set(cVLSM+value)
        else:
            p[f'sbnunu::Re{{{key}}}'].set(value)

    obs = [eos.Observable.make(
                    'B->Knunu::BR',
                    p,
                    eos.Kinematics(q2_min=q2min, q2_max=q2max),
                    eos.Options({'form-factors':'BSZ2015', 'model':'WET'}))
                    for q2min, q2max in zip(q2_pts[:-1], q2_pts[1:]) ]
    BR = np.array([o.evaluate() for o in obs])
    
    return BR

def computeBR(model={}, alpha = [], parameters = None):
    """Compute the branching ratio of the full kinematic range.

    Args:
        model: dictionary of WCs. Possible WCs: CVL, CVR, CSL, CSR, CTL
        alphas: array of FF parameters
        parameters: EOS parameters object
        
    Returns:
        total branching ratio    
    """
    return computedBR(model, [0., kinlim()], alpha, parameters)[0]

def computeWeights(model, q2_pts, alpha = [], norm=1, parameters = None):
    """
    Compute the FF weights as a function of the model, the FF parameters and the q2 bin edges

    Args:
        model: dictionary of WCs. Possible WCs: CVL, CVR, CSL, CSR, CTL
        q2_pts: q2 bin edges. Must be within kinematic limit of [0.0, ~22.9]
        alphas: array of FF parameters
        norm: normalization of the branhcing ratio to get the weights
        parameters: EOS parameters object

    Returns:
        array of weights in given binning 
    """
    BR = computedBR(model, q2_pts, alpha, parameters)
    weights = BR/norm
    weights[weights<=0] = 1
    weights[np.isnan(weights)] = 1
    return weights


###############################################################################################
# ANALYTIC

def lam(a,b,c):
    return a**2+b**2+c**2-2*(a*b+b*c+a*c)

def analyticDBR(q2):
    """
    Phase space computation:
    https://pdg.lbl.gov/2019/reviews/rpp2019-rev-kinematics.pdf
    Eq 47.22:
    * set Matrix element to 1
    * m1=m2=0, m3=mK, M=mB
    * m12=q
    * dGamma/dq2 propto dm23^2 = (m23^2)_max - (m23^2)_min (which are in eq 47.23)
    * This gives dGamma/dq2 propto sqrt(lambda)
    Args:
        q2 (_type_): _description_

    Returns:
        _type_: _description_
    """
    parameters  = eos.Parameters.Defaults()
    mB = parameters['mass::B_u'].evaluate()
    mK = parameters['mass::K_u'].evaluate()
    lam_BK = lam(mB**2, mK**2, q2)
    return np.sqrt(lam_BK)/( (2*np.pi*mB)**3 * 32)

def kinlim():
    """Calculate the kinematic limit of the B->Knunu decay, assuming massless neutrinos. 

    Returns:
        kinematic limit of the B->Knunu decay
    """
    parameters  = eos.Parameters.Defaults()
    mB = parameters['mass::B_u'].evaluate()
    mK = parameters['mass::K_u'].evaluate()
    kinlim = (mB-mK)**2
    return kinlim

@np_cache()
def computePHSP(q2_pts, norm = True):
    PHSP = bintegrate(analyticDBR, q2_pts)
    
    if norm: 
        PHSP = PHSP / np.sum(PHSP)
    return PHSP

def bintegrate(func, q2_pts, args=()):
    return np.array([sp.integrate.quad(func, q2min, q2max, args=args)[0] for q2min, q2max in zip(q2_pts[:-1], q2_pts[1:]) ]) 

###############################################################################################
# STATS

def pca(cov):
    """Principal Component analysis, moving to a space where the covariance matrix is diagonal
    https://www.cs.cmu.edu/~elaw/papers/pca.pdf

    Args:
        cov (array): Covariance matrix

    Returns:
        array: matrix of column wise error vectors (eigenvectors * sqrt(eigenvalues); sqrt(eigenvalues) = std)
    """
    svd = np.linalg.svd(cov)
    uvec = svd[0] @ np.sqrt(np.diag(svd[1]))
    return uvec
