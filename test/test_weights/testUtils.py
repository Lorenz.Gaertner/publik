import unittest
import numpy as np
import eos

from publik.weights import utils

class TestUtils(unittest.TestCase):
    nbins = 23
    q2_pts = np.linspace(0., 23, nbins+1) 
    q2_pts_cut = np.where(q2_pts < utils.kinlim(), q2_pts, utils.kinlim())
    # example parameters
    alpha= [ 0.33835466, -0.87573043, -0.09009672,  0.378397  ,  0.08594659, 0.32784709, -0.94625345, -0.23942452]
    model = {'cVL': 0.0}
    
    def test_kinlim(self):
        self.assertAlmostEqual(utils.kinlim(), 22.902570349569004, 8)
        
    def test_lam(self):
        self.assertEqual(utils.lam(6,7,8), -143)
        
    def test_analyticDBR(self):
        q2 = np.linspace(0,utils.kinlim(), 5)
        expected = [2.36546469e-05, 1.86429739e-05, 1.35514872e-05, 8.23342669e-06, 0.00000000e+00]
        for q, e in zip(q2, expected):
            self.assertAlmostEqual(utils.analyticDBR(q), e, 8)
            
    def test_sampledBR(self):
        model = {"cVL": 0.0}
        q2_pts = np.linspace(0,utils.kinlim(), 5)
        par, sam = utils.sampledBR(q2_pts, model=model)
        
        samM = np.mean(sam, axis=0)
        samE = np.std(sam, axis=0)
        parM = np.mean(par, axis=0)
        parE = np.std(par, axis=0)

        exp_samM = np.array([1.68448567e-06, 1.51851688e-06, 1.16503328e-06, 4.54027815e-07])
        exp_samE = np.array([1.14103859e-07, 5.65159801e-08, 3.67932761e-08, 1.40924839e-08])
        exp_parM = np.array([ 0.34081827, -0.84617464, -0.01788649,  0.39868965,  0.12339778,  0.33160637, -0.91010056, -0.16309497])
        exp_parE = np.array([0.01528031, 0.18695103, 0.47766406, 0.12742117, 0.24311874, 0.02274505, 0.20811449, 0.47686582])
        
        for diff, err in zip(samM-exp_samM, np.sqrt(exp_samE**2 + samE**2)):
            self.assertLess(diff, err*5)
            
        for diff, err in zip(parM-exp_parM, np.sqrt(exp_parE**2 + parE**2)):
            self.assertLess(diff, err*5)
            
    def test_computedBR(self):
        BR = utils.computedBR(self.model, self.q2_pts_cut, alpha = self.alpha)
        
        self.assertEqual(len(BR), len(self.q2_pts_cut)-1)
        #comapre SM vs WET
        analysis = utils.analysis()
        BRSM  = np.array([eos.Observable.make(
                                'B->Knunu::BR',
                                analysis.parameters,
                                eos.Kinematics(q2_min=q2min, q2_max=q2max),
                                eos.Options({'form-factors':'BSZ2015', 'model':'SM'})).evaluate()
                            for q2min, q2max in zip(self.q2_pts_cut[:-1], self.q2_pts_cut[1:]) ])
        
        alpha = [
            analysis.parameters['B->K::alpha^f+_0@BSZ2015'].evaluate(),
            analysis.parameters['B->K::alpha^f+_1@BSZ2015'].evaluate(),
            analysis.parameters['B->K::alpha^f+_2@BSZ2015'].evaluate(),
            analysis.parameters['B->K::alpha^f0_1@BSZ2015'].evaluate(),
            analysis.parameters['B->K::alpha^f0_2@BSZ2015'].evaluate(),
            analysis.parameters['B->K::alpha^fT_0@BSZ2015'].evaluate(),
            analysis.parameters['B->K::alpha^fT_1@BSZ2015'].evaluate(),
            analysis.parameters['B->K::alpha^fT_2@BSZ2015'].evaluate(),
        ]
        
        BR = utils.computedBR(self.model, self.q2_pts_cut, alpha = alpha)
        
        self.assertEqual(len(BR), len(self.q2_pts_cut)-1)
        np.testing.assert_allclose(BR, BRSM, rtol=1e-6)
        
    def test_computeWeights(self):
        weights = utils.computeWeights(self.model, self.q2_pts_cut, alpha = self.alpha)
        
        self.assertEqual(len(weights), len(self.q2_pts_cut)-1)
            
    def test_computeBR(self):
        model = {'cVL': 0}
        self.assertAlmostEqual(utils.computeBR(model), 4.811577782694307e-06, 8)
        model = {'cVL': 10}
        self.assertAlmostEqual(utils.computeBR(model), 3.036581119232672e-05, 8)
        model = {'cSL': 10}
        self.assertAlmostEqual(utils.computeBR(model), 3.632833028608578e-05, 8)
        model = {'cTL': 10}
        self.assertAlmostEqual(utils.computeBR(model), 5.084613496773898e-05, 8)
        
    def test_computePHSP(self):
        PHSP = utils.computePHSP(self.q2_pts_cut)
        PHSPref = np.array([   0.07613638, 0.07327545, 0.07041023, 0.06754015, 0.06466458,
                            0.06178275, 0.05889374, 0.05599643, 0.05308947, 0.05017118,
                            0.04723945, 0.04429161, 0.04132421, 0.0383327 , 0.03531093,
                            0.03225038, 0.02913877, 0.02595768, 0.02267767, 0.01924772,
                            0.0155673 , 0.01138661, 0.00531459])

        np.testing.assert_allclose(PHSP, PHSPref, rtol=1e-6)
        
    def test_bintegrate(self):
        pts = np.linspace(0, 10, 11)
        
        const = utils.bintegrate(lambda x: 1, pts)
        const_test = np.array([1., 1., 1., 1., 1., 1., 1., 1., 1., 1.])
        np.testing.assert_allclose(const, const_test, rtol=1e-8)
        
        lin = utils.bintegrate(lambda x: x, pts)
        lin_test = np.array([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5])
        np.testing.assert_allclose(lin, lin_test, rtol=1e-8)
        
        exp = utils.bintegrate(lambda x: np.exp(x), pts)
        exp_test = np.array([1.71828183e+00, 4.67077427e+00, 1.26964808e+01, 3.45126131e+01,
                            9.38150091e+01, 2.55015634e+02, 6.93204365e+02, 1.88432483e+03,
                            5.12212594e+03, 1.39233819e+04])
        np.testing.assert_allclose(exp, exp_test, rtol=1e-8)
            
    def test_principalComponentAnalysis(self):
        cov =   [[ 1.21000e-04,  3.37920e-04, -2.80830e-03],
                [ 3.37920e-04,  9.21600e-03,  1.72224e-02],
                [-2.80830e-03,  1.72224e-02,  4.76100e-01]]
        pa = utils.pca(cov).T
        np.testing.assert_allclose(cov, pa.T.dot(pa), rtol=1e-8)
        
    def test_parameterReset(self):
        a = utils.analysis()
        a_default = utils.analysis()
        
        a.parameters['sbnunu::Re{cVL}'].set(1000)
        utils.parameterReset(a.parameters, a_default.parameters)
        
        self.assertEqual(a.parameters['sbnunu::Re{cVL}'].evaluate(),
                         a_default.parameters['sbnunu::Re{cVL}'].evaluate())


if __name__ == '__main__':
    unittest.main()