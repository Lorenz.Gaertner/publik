import os
from pathlib import Path

import unittest
import numpy as np
import pickle
import json
import pyhf

from publik.reweight import utils

class TestUtils(unittest.TestCase):
    filename = Path( os.path.dirname(__file__)) / 'pyhf_publication_B_K_nunu.json'
                        
    def test_getPDF(self):
        # test if we can correctly read pyhf inputs
        utils.getPDF(self.filename)
        
    def test_calculate_pulls_wrt_nominal(self):
        pdf, data = utils.getPDF(self.filename)
        pyhf.set_backend("numpy", pyhf.optimize.scipy_optimizer())
        
        best_fit = pyhf.infer.mle.fit(data, 
                                         pdf, 
                                         pdf.config.suggested_init(), 
                                         pdf.config.suggested_bounds())
        pulls = utils.calculate_pulls_wrt_nominal(pdf, best_fit)
        np.testing.assert_array_less(np.abs(pulls), np.ones(len(pulls)))
  
if __name__ == '__main__':
    unittest.main()