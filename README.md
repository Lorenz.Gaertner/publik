# Code for reinterpreting the BELLEII  $B \to K \nu \bar{\nu}$ analysis in terms of BSM models


## Installation
To use the code provided here, one can clone and pip install it locally with:
```bash
git clone git@gitlab.desy.de:lorenz.gaertner/publik.git
cd publik
pip install -e ./
```
To install the required packages:
```bash
pip install -r requirements.txt
```

## $B \to K \nu \bar{\nu}$ at BELLEII

### Publication
[PhysRevLett.127.181802](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.127.181802)

### HEPData entry
[HEPData](https://www.hepdata.net/record/ins1860766)

### Repository links
* [Publication repository](https://stash.desy.de/users/lorenzg/repos/b2hnn_publication/browse) forked from [original repository](https://stash.desy.de/users/sstefkov/repos/b2hnn_publication/browse)
* [Updated analysis](https://gitlab.desy.de/lorenz.gaertner/b-2-hadronnunubar-with-inclusive-tagging-new) forked from [original repository](https://gitlab.desy.de/cyrille.praz/b2hadronnunubar_with_inclusive_tagging)

## Getting started
A minimal fitting example of the code is given in 
```
applications/fitting_example.ipynb
```
To investigate how the yields change as afunction of Wilson coefficients, try
```
applications/plot_yields.ipynb
```