import os
from pathlib import Path

import numpy as np
import json

from publik.weights import plot 
from publik.weights import utils

import multiprocessing as mp

BASE = Path(os.path.dirname(__file__))
# INPUT =         (BASE / 'inputs/scan.json').resolve()
# WEIGHTDIR =     (BASE / '../../results/weights/weightfiles/').resolve()
# FIGDIR =        (BASE / '../../results/weights/figs/').resolve()
# BRDIR =         (BASE / '../../results/weights/brs/').resolve()
# SAMPLEDIR =     (BASE / '../../results/weights/samples/').resolve()

INPUT =         (BASE / 'inputs/wilson.json').resolve()
WEIGHTDIR =     (BASE / '../../results/TEST/').resolve()
FIGDIR =        (BASE / '../../results/TEST/').resolve()
BRDIR =         (BASE / '../../results/TEST/').resolve()
SAMPLEDIR =     (BASE / '../../results/TEST/').resolve()


#BINNING 
nbins = 23
q2_pts = np.linspace(0., 23, nbins+1) 
q2_pts_cut = np.where(q2_pts < utils.kinlim(), q2_pts, utils.kinlim())
    
def weightsBin(model, q2_pts, norm, return_sam=False):
    """Compute the weights per q2 bin

    Args:
        model (dict): Dictionary setting the Wilson coefficients. 
                Possible entires: 'cVL', 'cVR', 'cSL', 'cSR', 'cTL'
        q2_pts_cut (array): bin edges in range 0<q2<kinlim
        norm: weight normalization, PHSP (phase space) * BRSM (SM normalization)
        return_sam (bool, optional): return the samples and parameters
        
    Returns:
        _type_: weights and uncertainties in specified bins
    """
    
    q2_pts_cut = np.where(q2_pts < utils.kinlim(), q2_pts, utils.kinlim())
    q2_pts_cent =  (q2_pts[:-1] + q2_pts[1:])/2
    
    # integrate and average
    par, sam = utils.sampledBR(q2_pts_cut, model=model)  
    
    #normalize to SM and PHSP
    samnorm = sam/norm
    
    weights = np.mean(samnorm, axis=0)
    weights[weights<=0] = 1
    weights[np.isnan(weights)] = 1

    parmean = np.mean(par, axis=0)
    cov = np.cov(par.T)

    uvec = utils.pca(cov)

    up = []
    down = []
    for v in uvec:
        u = utils.computeWeights(model, q2_pts_cut, alpha = parmean+v, norm=norm)
        d = utils.computeWeights(model, q2_pts_cut, alpha = parmean-v, norm=norm)
        up.append(u.tolist())
        down.append(d.tolist())  

    data = {
        'model': model,
        'q2':       q2_pts_cent.tolist(),
        'weights':  weights.tolist(),
        'corrFF': {'up': up, 'down': down},
        }
    if return_sam:
        return data, sam, par
    else:
        return data
    
def saveWeightsBin(model, PHSP, BRSM, saveBR=False):
    print(model)
        
    id = '_'.join(f'{key}={value:.2e}' for key, value in model.items())
    output = WEIGHTDIR / f'weight_{id}.json'
    
    #if file exists, skip
    if os.path.exists(output): return
    
    if saveBR:
        data, sam, par= weightsBin(model, q2_pts, PHSP * BRSM, return_sam=True)
        mean = data['weights'] * PHSP
        std = np.std(sam/BRSM, axis=0)
        samnorm = sam / BRSM
        with open(BRDIR / f'mean_{id}.json', 'w') as ff:
            jstr = json.dumps({'id':id, 'mean':mean.tolist(), 'std':std.tolist(), 'total': utils.computeBR(model)}, indent=4)
            ff.write(jstr)
        with open(SAMPLEDIR / f'sample_{id}.json', 'w') as ff:
            jstr = json.dumps({'id':id, 'samples':samnorm.tolist(), 'parameters':par.tolist(), 'weights': (samnorm / PHSP).tolist()}, indent=4)
            ff.write(jstr)
    else:
        data = weightsBin(model, q2_pts, PHSP * BRSM)

    # save as json
    with open(output, 'w') as ff:
        jstr = json.dumps(data, indent=4)
        ff.write(jstr)
        
    plot.weight(FIGDIR / f'ff_{id}_bin.pdf', id, q2_pts, data['weights'], data['corrFF']['up'], data['corrFF']['down'])
        

if __name__ == '__main__':  
    #DEFINE MODELS
    with open(INPUT, 'r') as f:
        models = json.load(f)
    print(f'Models to process: {len(models)}')
    
    #MC generator data
    # q2_gen, B_sig_K_pt = utils.mcdata(BASE / 'mcsig.root')
    
    #Analytic normalization
    PHSP = utils.computePHSP(q2_pts_cut)
    
    #SM normalization
    BRSM  = utils.computeBR()
    
    args = [(model, PHSP, BRSM, True) for model in models]
    for arg in args:
        saveWeightsBin(*arg)
    
