#!/usr/bin/env python3

from distutils.core import setup

setup(name='publik',
      version='0.0',
      author='Lorenz Gaertner',
      packages=['publik'],
     )