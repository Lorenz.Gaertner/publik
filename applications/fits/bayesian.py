import numpy as np

import pyhf
import eos

from publik.reweight import utils
from publik.weights import utils as wutils
import baseclass

#BUG
# We sample the hadronic parameters but we set the pca shifts.

def analysis():
    """
    Specify the likelihoods and FF parameter ranges 
    
    Returns:
        EOS analysis instance
    """

    # form factor expansion f+_0,1,2 are expansion parameters up to 2nd order
    # there is no f0_0 because of a constriant which removes one parameter

    parameters = [0.33772497529184886, -0.87793473613271, -0.07935870922121949, 0.3719622997220613, 0.07388594710238389, 0.327935912834808, -0.9490004115927961, -0.23146429907794228]
    paramerror = [0.010131234226468245, 0.09815140228051167, 0.26279803480131697, 0.07751034526769873, 0.14588095119443809, 0.019809720318176644, 0.16833757660616938, 0.36912754148836896]
    sigma = 15
    analysis_args = {
        'priors': [
            { 'parameter': 'sbnunu::Re{cVR}', 'min': -30., 'max': 30., 'type': 'uniform' },
            { 'parameter': 'sbnunu::Re{cSL}', 'min': -30., 'max': 30., 'type': 'uniform' },
            { 'parameter': 'sbnunu::Re{cTL}', 'min': -30., 'max': 30., 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f+_0@BSZ2015', 'min': parameters[0]-sigma*paramerror[0], 'max': parameters[0]+sigma*paramerror[0], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f+_1@BSZ2015', 'min': parameters[1]-sigma*paramerror[1], 'max': parameters[1]+sigma*paramerror[1], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f+_2@BSZ2015', 'min': parameters[2]-sigma*paramerror[2], 'max': parameters[2]+sigma*paramerror[2], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f0_1@BSZ2015', 'min': parameters[3]-sigma*paramerror[3], 'max': parameters[3]+sigma*paramerror[3], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^f0_2@BSZ2015', 'min': parameters[4]-sigma*paramerror[4], 'max': parameters[4]+sigma*paramerror[4], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^fT_0@BSZ2015', 'min': parameters[5]-sigma*paramerror[5], 'max': parameters[5]+sigma*paramerror[5], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^fT_1@BSZ2015', 'min': parameters[6]-sigma*paramerror[6], 'max': parameters[6]+sigma*paramerror[6], 'type': 'uniform' },
            { 'parameter': 'B->K::alpha^fT_2@BSZ2015', 'min': parameters[7]-sigma*paramerror[7], 'max': parameters[7]+sigma*paramerror[7], 'type': 'uniform' },
        ],
        'likelihood': [
            'B->K::f_0+f_++f_T@FLAG:2021A',
            'B->K::f_0+f_++f_T@HPQCD:2022A'
        ]
    }

    analysis = eos.Analysis(**analysis_args)
    analysis.optimize()
    return analysis

class bayesian(baseclass.fits_base_):
    def __init__(self, N):
        super().__init__()
        
        par, _ = wutils.sampledBR(self.q2binning_cut, analysis=analysis(), N_sample = N)
        
        self.INPUTS = {k:v for k,v in enumerate(par)}
        
        self.OUTDIR = self.BASE / 'results/fit/bayesian/'
    
    def results(self, par):
        pdf, data = utils.getPDF(self.PUBJSON, validate=False, modifier_set=self.expanded_pyhf)
        
        pyhf.set_backend("numpy", pyhf.optimize.scipy_optimizer())
        pdf.config.par_map['mu']['paramset'].suggested_bounds=[(-1000,1000)]

        #fix ff params and WCs to sample values
        fixed = np.full(len(pdf.config.par_names), False)
        fixed[pdf.config.poi_index]=True
        for key in self.new_params.keys():
            fixed[pdf.config.par_map[key]['slice']]=[True]
            
        init_pars = pdf.config.suggested_init()
        for ind, key in enumerate(self.new_params.keys()):
            init_pars[pdf.config.par_map[key]['slice']] = [par[ind]]

        par_bounds = pdf.config.suggested_bounds()

        best_fit, twice_nll = pyhf.infer.mle.fit(data, 
                                                pdf, 
                                                init_pars, 
                                                par_bounds,
                                                fixed.tolist(),
                                                return_fitted_val=True)
        best_fit_dict = {k: best_fit[v["slice"]].tolist() for k, v in pdf.config.par_map.items()}

        results = { 'twice_nll': float(twice_nll), 
                    'fit_values': best_fit_dict}
        return results