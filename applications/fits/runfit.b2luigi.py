import b2luigi

import runfit

# import bestfits
import hypotest
# import wilson
# import bayesian
# import bayesian_wcs
# import toys
# import sample_pymc


# TARGET = bestfits.bestfits()
TARGET = hypotest.hypotest()
# TARGET = bayesian.bayesian(10000)
# TARGET = bayesian_wcs.bayesian_wcs(30000)
# TARGET = toys.toys(10)
# TARGET = sample_pymc.sample_pymc(50000)

class Task(b2luigi.Task):
    id = b2luigi.Parameter()

    def output(self):
        # id = self.input.replace('_B_K_nunu', '')
        return b2luigi.LocalTarget(TARGET.OUTDIR / f'{self.id}.json')

    def run(self):
        input = TARGET.INPUTS[self.id]
        runfit.savefit(self.id, input, TARGET)


if __name__ == "__main__":
    # Choose htcondor as our batch system
    b2luigi.set_setting("batch_system", "htcondor")
    # Setup the correct environment on the workers
    b2luigi.set_setting("env_script", "luigisetup.sh")
    b2luigi.set_setting("executable", ["python"])
    # Where to store the results
    b2luigi.set_setting("result_dir", TARGET.OUTDIR / 'logs')
    # Where to store the logs
    b2luigi.set_setting("log_dir", TARGET.OUTDIR / 'logs')
    # Increase allowed time for one job if needed
    b2luigi.set_setting("htcondor_settings", 
                        {"request_memory": "80GB", 
                        "+RequestRuntime": 72000,
                        "Request_Cpus": 8}
                        )

    print(list(TARGET.INPUTS.keys()))
    b2luigi.process([Task(input) for input in list(TARGET.INPUTS.keys())],
                    workers=5000,
                    batch=True) 
