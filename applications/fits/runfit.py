import os
import json

import multiprocessing as mp

# import bestfits
# import cls
# import hypotest
# import wilson
# import bayesian
import bayesian_wcs

 
def savefit(id, input, target):
    output = target.OUTDIR / f'{id}.json'

    if os.path.exists(output): return
    
    results = target.results(input)
    
    with open(output, 'w') as ff:
        jstr = json.dumps(results, indent=4)
        ff.write(jstr)

if __name__ == '__main__':
    # TARGET = bestfits.bestfits()
    # TARGET = cls.cls()
    # TARGET = hypotest.hypotest()
    # TARGET = bayesian.bayesian(10000)
    TARGET = bayesian_wcs.bayesian_wcs(2)
    
    args = [(k, v, TARGET) for k,v in TARGET.INPUTS.items()]
    for arg in args:
        savefit(*arg)
    print("Done!")
    # with mp.Pool(8) as pool:
        # pool.starmap(savefit, args)
        # print("Done!")