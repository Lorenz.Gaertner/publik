import numpy as np
import json

import pyhf
from publik.reweight import utils
import baseclass

class bestfits(baseclass.fits_base_):
    def __init__(self):
        super().__init__()
        
        input = self.BASE / 'publik/weights/inputs/scan.json'
        
        with open(input, 'r') as f:
            models = json.load(f)
        
        ids = ['_'.join(f'{key}={value:.3e}' for key, value in model.items()) for model in models]
        self.INPUTS = {k:v for k,v in zip(ids, models)}
        
        self.OUTDIR =   self.BASE / 'results/fit/bestfits/'
    
    def results(self, model):
        pdf, data = utils.getPDF(self.PUBJSON, validate=False, modifier_set=self.expanded_pyhf)
        
        pyhf.set_backend("numpy", pyhf.optimize.scipy_optimizer())
        # pyhf.set_backend("numpy", pyhf.optimize.minuit_optimizer(verbose=True))
        
        #set WCs and bounds
        init_pars = pdf.config.suggested_init()
        init_pars[pdf.config.par_map['cvl']['slice']] = [model['cVL']]
        init_pars[pdf.config.par_map['csl']['slice']] = [model['cSL']]
        init_pars[pdf.config.par_map['ctl']['slice']] = [model['cTL']]
        
        par_bounds = pdf.config.suggested_bounds()
        pdf.config.par_map['mu']['paramset'].suggested_bounds=[(-1000,1000)]
        
        #fix WCs
        fixed = np.full(len(pdf.config.par_names), False)
        fixed[pdf.config.poi_index]=True
        fixed[pdf.config.par_map['cvl']['slice']]=[True]
        fixed[pdf.config.par_map['csl']['slice']]=[True]
        fixed[pdf.config.par_map['ctl']['slice']]=[True]
        
        best_fit, twice_nll = pyhf.infer.mle.fit(data, 
                                                pdf, 
                                                init_pars, 
                                                par_bounds, 
                                                fixed.tolist(),
                                                return_fitted_val=True)
        best_fit_dict = {k: best_fit[v["slice"]].tolist() for k, v in pdf.config.par_map.items()}
        
        results = { 'model': model,
                    'twice_nll': float(twice_nll), 
                    'fit_values': best_fit_dict}

        return results