import numpy as np
import pyhf

from publik.reweight import utils
import baseclass

def sampleWC(N=10000):
    """Uniform sampling of WCs

    Args:
        N (int, optional): Number of samples in WC space. Defaults to 10000.

    Returns:
        array: uniform samples in WC space
    """
    return np.random.uniform(-35., 35., size=(N, 3))

class bayesian_wcs(baseclass.fits_base_):
    def __init__(self, N):
        super().__init__()
        
        par = sampleWC(N)
        
        self.INPUTS = {k:list(v) for k,v in enumerate(par)}
        
        self.OUTDIR = self.BASE / 'results/fit/bayesian_wcs_wide/'
    
    def results(self, par):
        print('INPUT: ', par)
        pdf, data = utils.getPDF(self.PUBJSON, validate=False, modifier_set=self.expanded_pyhf)
        
        pyhf.set_backend("numpy", pyhf.optimize.scipy_optimizer())
        pdf.config.par_map['mu']['paramset'].suggested_bounds=[(-1000,1000)]

        #fix ff params and WCs to sample values
        fixed = np.full(len(pdf.config.par_names), False)
        fixed[pdf.config.poi_index]=True
        fixed[pdf.config.par_map['cvl']['slice']] = True
        fixed[pdf.config.par_map['csl']['slice']] = True
        fixed[pdf.config.par_map['ctl']['slice']] = True
        
        init_pars = pdf.config.suggested_init()
        init_pars[pdf.config.par_map['cvl']['slice']] = [par[0]]
        init_pars[pdf.config.par_map['csl']['slice']] = [par[1]]
        init_pars[pdf.config.par_map['ctl']['slice']] = [par[2]]

        par_bounds = pdf.config.suggested_bounds()

        best_fit, twice_nll = pyhf.infer.mle.fit(data, 
                                                pdf, 
                                                init_pars, 
                                                par_bounds,
                                                fixed.tolist(),
                                                return_fitted_val=True)
        best_fit_dict = {k: best_fit[v["slice"]].tolist() for k, v in pdf.config.par_map.items()}

        results = { 'twice_nll': float(twice_nll), 
                    'fit_values': best_fit_dict}
        return results