from pathlib import Path
import numpy as np

import publik
from publik.reweight import eos_modifier
from publik.weights import utils as wutils
from publik.reweight import utils as rwutils


class fits_base_():
    def __init__(self):
        self.OUTDIR =   None
        
        self.BASE = Path(publik.__file__).parents[1]
        self.PUBJSON =  self.BASE / 'pyhf_inputs/pyhf_Bplus2Kplus_v24_with_continuum_weights_no_ff_wilson.json'
        
        q2json = self.BASE / 'pyhf_inputs/pyhf_Bplus2Kplus_v24_with_continuum_weights_no_ff_q2.json'

        q2dist, q2binning = rwutils.getQ2Distributions(q2json)
        self.q2binning_cut = np.where(q2binning > 0, q2binning, 0)
        self.q2binning_cut = np.where(self.q2binning_cut < wutils.kinlim(), self.q2binning_cut, wutils.kinlim())
    
        
        self.expanded_pyhf = eos_modifier.add(self.q2binning_cut, q2dist)
    
    def results(self):
        pass