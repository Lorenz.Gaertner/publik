import numpy as np
import scipy as sp
import json

import pyhf
from publik.reweight import utils
import baseclass

class hypotest(baseclass.fits_base_):
    def __init__(self):
        super().__init__()
        
        input = self.BASE / 'publik/weights/inputs/scan.json'
        
        with open(input, 'r') as f:
            models = json.load(f)
        
        ids = ['_'.join(f'{key}={value:.3e}' for key, value in model.items()) for model in models]
        self.INPUTS = {k:v for k,v in zip(ids, models)}
        
        self.OUTDIR =   self.BASE / 'results/fit/hypo_profiled_wide/'
        
    def pvalue_to_significance(self, pvalue):
        return sp.stats.norm.isf(pvalue)
    
    def results(self, model):
        pdf, data = utils.getPDF(self.PUBJSON, validate=False, modifier_set=self.expanded_pyhf)
        
        pyhf.set_backend("numpy", pyhf.optimize.scipy_optimizer())
        # pyhf.set_backend("numpy", pyhf.optimize.minuit_optimizer(verbose=True))
        
        #set bounds
        pdf.config.par_map['mu']['paramset'].suggested_bounds=[(-1000,1000)]
        par_bounds = pdf.config.suggested_bounds()
        
        #set WCs and fix parameters
        init_pars = pdf.config.suggested_init()
        fixed = np.full(len(pdf.config.par_names), False)
        
        for k, v in model.items():   
            init_pars[pdf.config.par_map[k]['slice']] = [v]
            fixed[pdf.config.par_map[k]['slice']]=[True]
            
        best_fit, twice_nll = pyhf.infer.mle.fit(data, 
                                                pdf, 
                                                init_pars, 
                                                par_bounds,
                                                fixed.tolist(),
                                                return_fitted_val=True)
        best_fit_dict = {k: best_fit[v["slice"]].tolist() for k, v in pdf.config.par_map.items()}
        
        mu_test = 1.

        cls_obs, tail_probs, cls_exp = pyhf.infer.hypotest(
            mu_test,
            data,
            pdf,
            init_pars, 
            par_bounds, 
            fixed.tolist(),
            return_expected_set=True,
            return_tail_probs=True,
            test_stat='q'
        )    
        sig = self.pvalue_to_significance(cls_obs)
        
        asymptotic_calculator = pyhf.infer.calculators.AsymptoticCalculator(data, 
                                                                            pdf, 
                                                                            test_stat='q', 
                                                                            init_pars=init_pars, 
                                                                            par_bounds=par_bounds,
                                                                            fixed_params=fixed.tolist())

        sqrtqmu_v = asymptotic_calculator.teststatistic(mu_test)
        sqrtqmuA_v = asymptotic_calculator.sqrtqmuA_v
        
        tmu = pyhf.infer.test_statistics.tmu(mu_test, data, pdf, init_pars, par_bounds, fixed.tolist())
        
        asimov_data = pyhf.infer.calculators.generate_asimov_data(0., data, pdf, init_pars, par_bounds, fixed.tolist())
        tmuA = pyhf.infer.test_statistics.tmu(mu_test, asimov_data, pdf, init_pars, par_bounds, fixed.tolist())
        
        results = { 'model': model,
                    'cls_obs': float(cls_obs), 
                    'tail_probs': [float(t) for t in tail_probs],
                    'cls_exp': [float(c) for c in cls_exp],
                    'qmu': float(sqrtqmu_v), 
                    'qmuA': float(sqrtqmuA_v),
                    'tmu': float(tmu), 
                    'tmuA': float(tmuA),
                    'significance': float(sig),
                    'twice_nll': float(twice_nll), 
                    'fit_values': best_fit_dict}

        return results