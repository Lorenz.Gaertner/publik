# Steps to increase nr of bins 

* Produce new extended uncertainty csv files
    * `/publik/b2hadronnunubar_with_inclusive_tagging_new/code/b2hnn_helpers/scripts/extendsystematics.py`
* change the 3d variable registries
    * `/publik/b2hadronnunubar_with_inclusive_tagging_new/code/b2hnn_helpers/scripts/b2hnn_helpers/variable_registry.py`
* save the new jsons with no ff weights for offres and Y4S
    * `/publik/b2hadronnunubar_with_inclusive_tagging_new/code/plotting/scripts/save_json_parallel.py`
* produce the new pyhf json files
    * `/publik/b2hadronnunubar_with_inclusive_tagging_new/code/plotting/scripts/json_to_pyhf.py`